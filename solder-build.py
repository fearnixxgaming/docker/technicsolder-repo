import sys
import os
from os import environ
from os import path

from zipfile import ZipFile
import shutil

import json

# Load environment variables
sourceDir = environ['SOLDER_SRC']
targetDir = environ['SOLDER_TARGET']
buildDir = environ['SOLDER_BUILD']

if not path.isdir(sourceDir):
    print('[ERR] Invalid source directory {}'.format(sourceDir))
    sys.exit(1)

if not path.isdir(targetDir):
    print('[ERR] Invalid target directory {}'.format(targetDir))
    sys.exit(1)

if not path.isfile('targets.json'):
    print('[ERR] Could not find target configuration!')
    sys.exit(1)

with open('targets.json') as confFile:
    config = json.load(confFile)

# Evaluate possible mod archives/files
print('[INFO] PHASE: Discover')
files = os.listdir(sourceDir)
candidates = []
for file in files:
    filePath = path.join(sourceDir, file)

    if path.isdir(filePath):
        continue

    ext = path.splitext(filePath)[1]

    if ext not in config['extensions']:
        print('[FINE] Skipping file for unmatched extension: {} - {}'.format(file, ext))
        continue

    if "-" not in filePath:
        print('[WARN] Skipping file for missing version separator: {} - {}'.format(file, ext))
        continue

    print('[FINER] Found candidate: {}'.format(file))
    candidates.append(file)

if len(candidates) <= 0:
    print('[INFO] No candidates found.')
    sys.exit(0)

# Register slugs and their target configuration
print('[INFO] PHASE: Prepare')
registry = dict()
for candidate in candidates:
    noExtension = path.basename(path.splitext(candidate)[0])
    parts = noExtension.split('-')
    count = len(parts)

    if count < 3:
        print('[ERROR] Invalid candidate name. Expected "<slug>-<mc-version>-<mod-version>-<extra...>". Got: "{}"'
              .format(noExtension))
        continue

    slug = parts[0].lower()
    mc_version = parts[1]
    mod_version = parts[2]
    mod_extra = "".join([e for i, e in enumerate(parts) if i > 2])

    if slug in config['slugs']:
        targetConf = dict(config['slugs'][slug])
    else:
        targetConf = dict(config['slugs']['__default__'])

    targetConf['slug'] = slug
    targetConf['mc_version'] = mc_version
    targetConf['mod_version'] = mod_version
    targetConf['mod_extra'] = mod_extra
    targetConf['source_name'] = candidate
    targetConf['path'] = path.join(sourceDir, candidate)

    if slug in registry:
        registry[slug].append(targetConf)
    else:
        registry[slug] = [targetConf]

# Build the new archives
print('[INFO] PHASE: Build')
if not path.isdir(buildDir):
    os.makedirs(buildDir)
for slug in registry:
    print('[INFO] Building slug: {}'.format(slug))
    for conf in registry[slug]:
        archiveName = "{}-{}-{}.zip".format(conf['slug'], conf['mc_version'], conf['mod_version'])
        archivePath = path.join(conf['slug'], archiveName)
        archiveRepoPath = path.join(targetDir, archivePath)
        conf['repo_path'] = archiveRepoPath

        if path.exists(archiveRepoPath):
            print('[FINE] \tNo need to re-build: {}'.format(archivePath))
            conf['deploy'] = False
            continue

        archiveBuildPath = path.join(buildDir, archiveName)

        with ZipFile(archiveBuildPath, 'w') as archive:
            for target in conf['targets']:
                target = target.format(
                    slug=conf['slug'],
                    mc_version=conf['mc_version'],
                    mod_version=conf['mod_version'],
                    mod_extra=conf['mod_extra'],
                    source_name=conf['source_name'],
                    source_path=conf['path']
                )

                print('[INFO] \tWriting target to archive: {}'.format(target))
                archive.write(
                    filename=conf['path'],
                    arcname=target
                )
            conf['archive'] = archiveBuildPath
            conf['deploy'] = True
    # endfor - conf #
# endfor - slug #

# Move the archives
print('[INFO] PHASE: Deploy')
if not path.isdir(targetDir):
    os.makedirs(targetDir)
for slug in registry:
    for conf in registry[slug]:
        if not conf['deploy']:
            continue

        print('[FINE] Deploying: {}'.format(conf['repo_path']))

        slugDir = path.dirname(conf['repo_path'])

        if not path.isdir(slugDir):
            os.makedirs(slugDir)

        shutil.move(
            src=conf['archive'],
            dst=conf['repo_path']
        )
    # endfor - conf #
# endfor - slug #

print('[INFO] PHASE: Done')
