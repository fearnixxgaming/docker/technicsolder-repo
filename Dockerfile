FROM python:3.4.9-alpine3.8

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apk update && apk upgrade \
    && apk add --no-cache --update curl ca-certificates openssl git maven tar bash sqlite \
	&& apk add --no-cache --update python py-pip \
	&& pip install wget requests \
    && adduser -D -h /home/container container

COPY ./entrypoint.sh /entrypoint.sh
COPY ./solder-build.py /usr/bin/solder-build.py
COPY ./targets.json /home/container/targets.example.json

USER container
ENV USER container
ENV HOME /home/container

WORKDIR /home/container

CMD ["/bin/bash", "/entrypoint.sh"]
