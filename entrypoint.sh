#!/usr/bin/env bash
# /entrypoint.sh
# # Default entrypoint file from Pterodactyl # #
sleep 5

cd /home/container

if [[ -z "$SOLDER_SRC" ]]; then
    SOLDER_SRC="/home/container/src"
fi

if [[ -z "$SOLDER_BUILD" ]]; then
    SOLDER_BUILD="/home/container/build"
fi

if [[ -z "$SOLDER_TARGET" ]]; then
    SOLDER_TARGET="/home/container/repository"
fi

export SOLDER_SRC
export SOLDER_BUILD
export SOLDER_TARGET

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
# Tipp: apache2 -D FOREGROUND
${MODIFIED_STARTUP}
